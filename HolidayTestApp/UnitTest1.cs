﻿using System;
using HolidaysWithCodility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HolidayTestApp
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IsEqualPeriodOfTime()
        {

            int[] holidays = new[] { 7, 3, 5, 6, 8, 3, 7, 3, 1, 3, 3, 1 };

            int[] holidaysOneVariant = new[] { 1, 1, 1 };

            int[] holidaysVariantWithTwoSides = new[] { 5,1,1, 1, 4 };

            Assert.AreEqual(7, CodilityTask.GetPeriodOfTimeWithUniqueLocation(holidays));

            Assert.AreEqual(1, CodilityTask.GetPeriodOfTimeWithUniqueLocation(holidaysOneVariant));

            Assert.AreEqual(5, CodilityTask.GetPeriodOfTimeWithUniqueLocation(holidaysVariantWithTwoSides));
        }

        [TestMethod]
        public void ReturnZeroIfArrayIsEmpty()
        {

            int[] holidays = new int[] {};

            Assert.AreEqual(0, CodilityTask.GetPeriodOfTimeWithUniqueLocation(holidays));

        }


        [TestMethod]
        public void PerformenceTestForHolidays()
        {

            int[] holidays = new int[100000];

            for (var i = 0; i < 100000; i++)
            {
                if (i == 10000 || i == 90000)
                {
                    holidays[i] = i;
                    continue;
                }

                holidays[i] = 4;
            }

            Assert.AreEqual(80001, CodilityTask.GetPeriodOfTimeWithUniqueLocation(holidays));

        }
    }
}
