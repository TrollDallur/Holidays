﻿using System.Collections.Generic;
using System.Linq;

namespace HolidaysWithCodility
{
    public class CodilityTask
    {
        public static int GetPeriodOfTimeWithUniqueLocation(int[] holidays)
        {

            if (holidays == null || holidays.Length == 0)
                return 0;

            int daysOnHoliday = holidays.Length;

            var data = new HashSet<int>(holidays);

            int numberOfUniqueLocations = data.Count();


            if (numberOfUniqueLocations == 1)
                return numberOfUniqueLocations;

            int result = daysOnHoliday;

            var holderForEndArray = 0;

            for (var i = 0; i < daysOnHoliday; i++)
            {


                var holidaysWithoutStartAndEndDuplications =
                    new HashSet<int>(holidays.Skip(i + 1).Take(daysOnHoliday - i).ToArray());

                if (holidaysWithoutStartAndEndDuplications.Count() == numberOfUniqueLocations)
                {
                    result -= 1;

                    ++holderForEndArray;
                }
                else
                {
                    holidaysWithoutStartAndEndDuplications = i == 0
                        ? new HashSet<int>(holidays.Skip(holderForEndArray).Take(daysOnHoliday - (i + 1)).ToArray())
                        : new HashSet<int>(holidays.Skip(holderForEndArray).Take(daysOnHoliday - i - 1).ToArray());

                    if (holidaysWithoutStartAndEndDuplications.Count() == numberOfUniqueLocations)
                    {
                        result -= 1;
                    }
                    else
                    {
                        return result;
                    }
                }
            }

            return result;
        }
    }
}
